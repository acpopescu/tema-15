package function;

import java.util.List;
import java.util.Map;

public class ParseResponse {
	public String get;
	public Map<String, Object> parameters;
	public Map<String, Object> errors;
	public int results;
	public List<History> response;
}

class History {
	public String continent;
	public String country;
	public int population;
	public Map<String, String> cases;
	public Map<String, String> deaths;
	public Map<String, String> tests;
	public String day;
	public String time;
}
