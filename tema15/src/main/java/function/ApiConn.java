package function;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import com.google.gson.Gson;

import java.util.SortedMap;
import java.util.TreeMap;
import java.io.IOException;

public class ApiConn {
	
	private static HttpRequest request;
	private static HttpResponse<String> response;
	
	private static SortedMap<String, Long> casesMap = new TreeMap<>();
	private static SortedMap<String, Long> recoveredMap = new TreeMap<>();
	private static SortedMap<String, Long> deathsMap = new TreeMap<>();
	private static boolean errors;
	
	private static String cases_new;
	private static String cases_active;
	private static String cases_critical;
	private static String cases_recovered;
	private static String cases_total;
	private static String deaths_new;
	private static String deaths_total;
	private static String tests_total;
	
	private static String Country;
	
	public void query(String url) {
		request = HttpRequest.newBuilder()
				.uri(URI.create("https://covid-193.p.rapidapi.com/history?country=" + url))
				.header("X-RapidAPI-Key", "49860d5f5amsh8298c7fe64960bdp1415c3jsna46808be1322")
				.header("X-RapidAPI-Host", "covid-193.p.rapidapi.com")
				.method("GET", HttpRequest.BodyPublishers.noBody())
				.build();
		try {
			response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
			parse();
		} catch (IOException | InterruptedException e) {
		  // handle exception
			errors = true;
		}
	}
	
	public void parse() {
		// Parse the JSON string
	    Gson gson = new Gson();
	    ParseResponse parseResponse = gson.fromJson(response.body(), ParseResponse.class);
	    
	    try {
	    	// Organizarea datelor necesare
	    	for (int i = 0; i < parseResponse.results; i++) {
	    		casesMap.put(parseResponse.response.get(i).day, Long.parseLong(parseResponse.response.get(i).cases.get("active")));
	    		recoveredMap.put(parseResponse.response.get(i).day, Long.parseLong(parseResponse.response.get(i).cases.get("recovered")));
	    		deathsMap.put(parseResponse.response.get(i).day, Long.parseLong(parseResponse.response.get(i).deaths.get("total")));
	    	}
	    	Country = parseResponse.response.get(0).country;
	    	cases_new = parseResponse.response.get(0).cases.get("new");
    		cases_active = parseResponse.response.get(0).cases.get("active");
    		cases_critical = parseResponse.response.get(0).cases.get("critical");
    		cases_recovered = parseResponse.response.get(0).cases.get("recovered");
    		cases_total = parseResponse.response.get(0).cases.get("total");
    		deaths_new = parseResponse.response.get(0).deaths.get("new");
    		deaths_total = parseResponse.response.get(0).deaths.get("total");
    		tests_total = parseResponse.response.get(0).tests.get("total");
	    } catch (Exception e) {
			  // handle exception
	    	errors = true;
		}
	}
	
	public String getCasesNew() {
		return cases_new;
	}
	
	public String getCasesActive() {
		return cases_active;
	}
	
	public String getCasesCritical() {
		return cases_critical;
	}
	
	public String getCasesRecovered() {
		return cases_recovered;
	}
	
	public String getCasesTotal() {
		return cases_total;
	}
	
	public String getDeathsNew() {
		return deaths_new;
	}
	
	public String getDeathsTotal() {
		return deaths_total;
	}
	
	public String getTestsTotal() {
		return tests_total;
	}
	
	public String getCountry() {
		return Country;
	}
	
	public SortedMap<String, Long> getResponseCountryCases() {
		return casesMap;
	}
	
	public SortedMap<String, Long> getResponseCountryRecovered() {
		return recoveredMap;
	}
	
	public SortedMap<String, Long> getResponseCountryDeaths() {
		return deathsMap;
	}
	
	public SortedMap<String, Long> getResponseCountryCasesInterval(String startKey, String endKey) {
		SortedMap<String, Long> sm = ((TreeMap<String, Long>) casesMap).subMap(startKey, endKey);
	    return sm;
	}
	
	public SortedMap<String, Long> getResponseCountryRecoveredInterval(String startKey, String endKey) {
		SortedMap<String, Long> sm = ((TreeMap<String, Long>) recoveredMap).subMap(startKey, endKey);
	    return sm;
	}
	
	public SortedMap<String, Long> getResponseCountryDeathsInterval(String startKey, String endKey) {
		SortedMap<String, Long> sm = ((TreeMap<String, Long>) deathsMap).subMap(startKey, endKey);
	    return sm;
	}
	
	public boolean getErrors() {
		return errors;
	}
	
	public void setErrors() {
		errors = false;
	}
}
